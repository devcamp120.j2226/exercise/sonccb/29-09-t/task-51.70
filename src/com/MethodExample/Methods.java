package com.MethodExample;
import java.util.ArrayList;

public class Methods {

    public static void retrieveLastIndexOfElement() {
        int[] array = new int[] {1,2,3,4,5};
        ArrayList<Object> arrayList = new ArrayList<Object>(5);
        for (int i = 0; i < array.length; i++) {
            arrayList.add(array[i]);
        }
        System.out.println("subtassk 1: Lấy vị trí xuất hiện cuối cùng của phần tử ");
        System.out.println( arrayList.get(arrayList.lastIndexOf(arrayList.size())));
        System.out.println("subtassk 2: Thêm một phần tử vào vị trí index");
        arrayList.add(6);
        System.out.println(arrayList);
        System.out.println("subtassk 3: Xoá object o khỏi ArrayList, object o này phải chứa trong ArrayList.[6] ");
        arrayList.add(new int[]{1,2,3,4});
        System.out.println(arrayList);
        arrayList.remove(6);
        System.out.println(arrayList);
        System.out.println("subtassk 4: Xoá một phần tử tại vị trí index [1]" );
        System.out.println(arrayList);
        arrayList.remove(1);
        System.out.println(arrayList);
        System.out.println("subtassk 5:  Cập nhật phần tử tại vị trí index.[0]");
        arrayList.set(0,10);
        System.out.println(arrayList);
        System.out.println("subtassk 6: Lấy vị trí index của object o trong ArrayList. [6]");
        int index = arrayList.indexOf(6);
        System.out.println(index);
        System.out.println("subtassk 7: Return object tại vị trí index trong ArrayList.[0]");
        System.out.println(arrayList.get(0));
        System.out.println("subtassk 8: lấy số lượng phần tử chứa trong ArrayList");
        System.out.println(arrayList.size());
        System.out.println("subtassk 9: Kiểm tra phần tử object o có chứa trong ArrayList, nếu có return true, ngược lại false.[10]");
        System.out.println(arrayList.contains(10));
        System.out.println("subtassk 10: Xoá tất cả các phần tử trong ArrayList");
        arrayList.clear();
        System.out.println(arrayList);
    }
}
